'''

contains the structure of each object

'''

import numpy as np
import config
# from config import x_fac, y_fac

class Object:

    '''Mario, Enemy, Floor, Boxes and Pillars '''

    def __init__(self, x, y, ch=config.EMPTY):
        '''thr x and y coords wrt top left of board'''
        self._x = x
        self._y = y
        self.x_2 = 0
        self._ch = ch
        self.structure = np.chararray((4, 4))
        self.structure[:, :] = self._ch
        self._type = config.TYPES[self._ch]

    def get_type(self):
        '''returns the type of Object eg: Mario, Enemy, etc '''
        return self._type

    def get_ch(self):
        '''returns the character corresponding to the object'''
        return self._ch

    def get_size(self):
        '''returns (height, width)'''
        return self.structure.shape

    def get_xy(self):
        '''Returns xy coordinates of the person/object'''
        return (self._x, self._y)

    def update_location(self, scene, new_x, new_y):
        '''update the location of the object'''

            # inital update will not clear original
        scene.clear_obj(self)
        self._x, self._y = new_x, new_y
        if self.get_ch() == config.ENEMY:
            scene.draw_obj(type(self)(new_x, new_y, self.x_2))
        else:
            scene.draw_obj(type(self)(new_x, new_y))


class Floor(Object):

    '''Defines the floor'''

    def __init__(self, x, y):
        super(Floor, self).__init__(x, y, config.FLOOR)



class Box(Object):

    '''Defines the powerup boxes'''

    def __init__(self, x, y):
        super(Box, self).__init__(x, y, config.BOX)


class Wall(Object):

    '''Defines the Pipes'''

    def __init__(self, x, y):
        super(Wall, self).__init__(x, y, config.WALL)


class Coin(Object):

    '''Defines the coins'''

    def __init__(self, x, y):
        super(Coin, self).__init__(x, y, config.COIN)


class Cloud(Object):

    '''Defines the coins'''

    def __init__(self, x, y):
        super(Cloud, self).__init__(x, y, config.CLOUD)
        temp_skel = np.matrix(
            [[config.EMPTY, config.CLOUD, config.CLOUD, config.EMPTY],
             [config.EMPTY, config.CLOUD, config.CLOUD, config.CLOUD],
             [config.CLOUD, config.CLOUD,
              config.CLOUD, config.CLOUD],
             [config.EMPTY, config.EMPTY, config.EMPTY, config.EMPTY]])
        self.structure[:, :] = temp_skel
        del temp_skel


class End(Object):

    '''Defines the coins'''

    def __init__(self, x, y):
        super(End, self).__init__(x, y, config.END)
        temp_skel = np.matrix(
            [[config.END, config.BOX, config.END, config.BOX],
             [config.BOX, config.END, config.BOX, config.END],
             [config.END, config.BOX,
              config.END, config.BOX],
             [config.BOX, config.END, config.BOX, config.END]])
        self.structure[:, :] = temp_skel
        del temp_skel
