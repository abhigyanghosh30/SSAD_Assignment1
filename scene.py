'''
The Scene Module
Renders the scene on each move and after 0.1s
'''

import sys
from os import system
import signal
import numpy as np
import colorama
from getch import _getChUnix

from objects import Wall, Coin, Cloud, End, Floor, Box

import config




class AlarmException(Exception):
    '''Raises an exception on Alarm signal'''
    pass


def setlevel1(scene):

    ''' Object Positions in Level 1 '''

    for i in range(0, 45, 2):
        scene.draw_obj(Floor(i, 28))
    scene.draw_obj(Floor(32, 20))
    scene.draw_obj(Floor(48, 24))
    scene.draw_obj(Floor(52, 20))
    scene.draw_obj(Floor(52, 24))
    for i in range(56, 109, 2):
        scene.draw_obj(Floor(i, 28))
    for i in [112, 114, 116, 120, 124]:
        scene.draw_obj(Floor(i, 20))
    scene.draw_obj(Wall(64, 24))
    scene.draw_obj(Wall(76, 24))
    scene.draw_obj(Wall(76, 20))
    scene.draw_obj(Wall(96, 20))
    scene.draw_obj(Wall(96, 24))

    scene.draw_obj(Coin(128, 16))
    scene.draw_obj(Box(128, 24))
    for i in range(128, 150, 4):
        scene.draw_obj(Floor(i, 28))

    scene.draw_obj(Box(36, 20))
    for i in np.random.randint(1, 150, 10):
        scene.draw_obj(Cloud(i, 0))

    for i in range(0, 31, 4):
        scene.draw_obj(End(150, i))



def setlevel2(scene):
    ''' Object Positions in Level 2 '''
    for i in range(0, 45, 2):
        scene.draw_obj(Floor(i, 28))
    scene.draw_obj(Floor(32, 20))
    scene.draw_obj(Floor(48, 24))
    scene.draw_obj(Floor(52, 20))
    scene.draw_obj(Floor(52, 24))
    for i in range(56, 109, 2):
        scene.draw_obj(Floor(i, 28))
    for i in [112, 114, 116, 120, 124]:
        scene.draw_obj(Floor(i, 20))
    scene.draw_obj(Wall(64, 24))
    scene.draw_obj(Wall(76, 24))
    scene.draw_obj(Wall(76, 20))
    scene.draw_obj(Wall(96, 20))
    scene.draw_obj(Wall(96, 24))

    scene.draw_obj(Coin(128, 16))
    scene.draw_obj(Box(128, 24))
    scene.draw_obj(Floor(128, 28))
    scene.draw_obj(Floor(132, 28))
    scene.draw_obj(Floor(136, 28))
    scene.draw_obj(Box(36, 20))
    for i in np.random.randint(1, 150, 10):
        scene.draw_obj(Cloud(i, 0))

    for i in range(0, 31, 4):
        scene.draw_obj(End(150, i))


class Scene:

    '''Each scene consists of a 44 x_coord  20 character representation'''

    def __init__(self, level=1):
        self.level = level
        self.map = np.chararray((32, 200))
        self.map[:, :] = config.EMPTY
        self.storage = {
            config.FLOOR: [],
            config.WALL: [],
            config.BOX: [],
            config.ENEMY: [],
            config.MARIO: [],
            config.COIN: [],
            config.CLOUD: [],
            config.END: []
        }
        if level == 1:
            setlevel1(self)
        else:
            setlevel2(self)
        self.loe = []
        self.width = 44
        self.height = 32
        self.frame = np.chararray((32, 44))
        self.frame = self.map[0:32, 0:44]

    def draw_obj(self, obj):
        '''Draws the Object in the Level'''
        x_coord, y_coord = obj.get_xy()
        try:
            for i in range(0, 4):
                for j in range(0, 4):
                    self.storage[obj.get_ch()].append((x_coord + i, y_coord + j))
        except:
            pass
        self.map[y_coord:y_coord + 4, x_coord:x_coord + 4] = obj.structure

    def clear_obj(self, obj):
        '''removes an object from the board'''
        x_coord, y_coord = obj.get_xy()
        try:
            for i in range(0, 4):
                for j in range(0, 4):
                    self.storage[obj.get_ch()].remove((x_coord + i, y_coord + j))
        except:
            pass
        self.map[y_coord:y_coord + 4, x_coord:x_coord + 4] = config.EMPTY

    def update_scene(self):
        '''check changes in position of Mario everytime and updates all elements accordingly'''
        # check Mario position
        for x_coord, y_coord in self.storage[config.FLOOR]:
            self.map[y_coord, x_coord] = config.FLOOR
        for x_coord, y_coord in self.storage[config.WALL]:
            self.map[y_coord, x_coord] = config.WALL
        for i in range(0, len(self.storage[config.CLOUD]), 16):
            x_coord, y_coord = self.storage[config.CLOUD][i]
            self.map[y_coord:y_coord + 4, x_coord:x_coord + 4] = Cloud(0, 0).structure[:, :]
        temp = self.storage[config.MARIO[0]]
        m_x = temp[0][0]
        self.frame = self.map[0:32, m_x - 20:m_x + 24]

    def process_input(self, mario):
        '''process the user input'''
        def alarmhandler(signum, frame):
            ''' input method '''
            raise AlarmException

        def user_input(timeout=0.1):
            ''' input method '''
            signal.signal(signal.SIGALRM, alarmhandler)
            signal.setitimer(signal.ITIMER_REAL, timeout)
            try:
                text = _getChUnix()()
                signal.alarm(0)
                return text
            except AlarmException:
                pass
            signal.signal(signal.SIGALRM, signal.SIG_IGN)
            return ''

        key_press = user_input()

        if key_press == 'a':
            m_x, m_y = self.storage[config.MARIO[0]][0]
            flag = 0
            if m_x > 20:
                for i in range(0, 4):
                    if (m_x - 1, m_y + i) in self.storage[config.FLOOR] or (m_x - 1, m_y + i) in self.storage[config.WALL]:
                        flag = 1
                        break
                if flag == 0:
                    mario.update_location(self, m_x - 4, m_y)
        elif key_press == 'd':
            m_x, m_y = self.storage[config.MARIO[0]][0]
            flag = 0
            for i in range(0, 4):
                if (m_x + 4, m_y + i) in self.storage[config.FLOOR] or (m_x + 4, m_y + i) in self.storage[config.WALL]:
                    flag = 1
            if flag == 0:
                mario.update_location(self, m_x + 4, m_y)
        elif key_press == 'w':
            # initiates the multicycle jump for mario
            m_x, m_y = self.storage[config.MARIO[0]][0]
            if m_x == 0:
                return 0
            if mario.jump == 0:
                if (m_x, m_y - 1) not in self.storage[config.FLOOR] and (m_x, m_y - 1) not in self.storage[config.WALL]:
                    mario.jump = 4
                    system('aplay bigjump.wav> /dev/null 2>&1 &')
        elif key_press == 'q':
            return -10

        # if mario has ability to jump and nothing is above him, he can jump
        if mario.jump > 0:
            m_x, m_y = self.storage[config.MARIO[0]][0]
            if (m_x, m_y - 1) not in self.storage[config.FLOOR] and (m_x, m_y - 1) not in self.storage[config.WALL]:
                mario.update_location(self, m_x, m_y - 1)
                mario.jump -= 1
            else:
                mario.jump = 0

        # if mario is at the top, he can't jump any higher
        m_x, m_y = self.storage[config.MARIO[0]][0]
        if m_y == 0:
            mario.jump = 0

        # if he lands and there is no footholding below, then he keeps dropping
        if mario.jump == 0 and (m_x, m_y + 4) not in self.storage[config.FLOOR] and (m_x, m_y + 4) not in self.storage[config.WALL]:
            if m_y == 28:
                mario.update_location(self, m_x, 0)
                return -1
            mario.update_location(self, m_x, m_y + 1)
            self.render()
            print(mario.LIVES)
            print(mario.get_xy())

        # check if mario has opened any box
        if mario.get_xy() in self.storage[config.BOX]:
            self.storage[config.BOX].remove(mario.get_xy())
            mario.score += 200

        if mario.get_xy() in self.storage[config.COIN]:
            # system('aplay coin.wav> /dev/null 2>&1 ')
            self.storage[config.COIN].remove(mario.get_xy())
            mario.score += 100

        return 0

    def moveenemies(self):
        ''' movement of enemies '''
        for enemy in self.loe:
            x_coord, y_coord = enemy.get_xy()
            if enemy.dir == 'r':
                enemy.update_location(self, x_coord + 1, y_coord)
                if enemy.boss:
                    enemy.update_location(self, x_coord + 1, y_coord)
            else:
                enemy.update_location(self, x_coord - 1, y_coord)
                if enemy.boss:
                    enemy.update_location(self, x_coord - 1, y_coord)

            if x_coord == enemy.x_2 and enemy.dir == 'r':
                enemy.dir = 'l'
            elif x_coord == enemy.x_1 and enemy.dir == 'l':
                enemy.dir = 'r'
            newx, newy = enemy.get_xy()
            m_x, m_y = self.storage[config.MARIO[0]][0]
            if abs(newx - m_x) <= 2 and abs(newy - m_y) <= 2:
                if m_y < newy and not enemy.boss:
                    self.clear_obj(enemy)
                    self.loe.remove(enemy)
                    return 1
                if enemy.boss and m_y < newy:
                    enemy.LIVES -= 1
                    if enemy.LIVES == 0:
                        self.clear_obj(enemy)
                        self.loe.remove(enemy)
                else:
                    return -1
        return 0

    def render(self):
        '''# displaying the board at every frame'''
        colorama.init()
        sys.stdout.flush()
        system('tput reset')

        temp_board = np.matrix(self.frame)
        for row in range(self.height):
            for col in range(self.width):
                sys.stdout.write(config.getcc((temp_board[row, col]).decode()))
            sys.stdout.write("\n")
        del temp_board
