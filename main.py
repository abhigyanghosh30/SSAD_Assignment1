''' The run file
# Instances of classes created here
# for running the application.'''
import time
import os
import colorama
import people
import config
import scene

def main():

    '''Game starts here incase you were wondering'''
    colorama.init()
    # the initial Welcome Print.
    print("Welcome, to this rendition of SuperMario!")
    newpid = os.fork()
    if newpid == 0:
        os.system('cvlc christmas_theme.ogg > /dev/null 2>&1 &')
    else:
        os.waitpid(newpid, 0)
        print("Press 1 for Level 1 and 2 for Level 2")
        level = input()
        if not level or int(level) >= 3:
            print("Seems like you cannot follow instructions and are a fool")
            print("So better start with level 1")
            level = 1
        new_scene = scene.Scene(int(level))
        print("Press 'Enter' to start playing.")
        input()

        mario = people.Mario(20, 0, config.LIVES[int(level) - 1])
        new_scene.draw_obj(mario)
        if int(level) == 1:
            new_scene.loe = [people.Enemy(24, 24, 40), people.Enemy(
                80, 24, 88), people.Enemy(100, 24, 112), people.Boss(132, 24, 140)]
        else:
            new_scene.loe = [people.Enemy(24, 24, 40), people.Enemy(
                80, 24, 88), people.Enemy(100, 24, 112), people.Boss(132, 24, 140)]

        for enemy in new_scene.loe:
            new_scene.draw_obj(enemy)
        starttime = time.time()
        while True:
            new_scene.update_scene()
            mvene = new_scene.moveenemies()
            if mvene == -1:
                mario.LIVES -= 1
                m_x = mario.get_xy()[0]
                mario.update_location(new_scene, m_x, 0)
            if mvene == 1:
                mario.score += 100
            new_scene.render()
            print(mario.LIVES)
            print(mario.score)
            uiput = new_scene.process_input(mario)
            if uiput < 0:
                if uiput == -10:
                    mario.LIVES = -1
                    break
                mario.LIVES -= 1
                os.system('aplay hurt.wav> /dev/null 2>&1')
                new_scene.render()
                print(mario.LIVES)
                print(mario.get_xy())
            if mario.LIVES <= 0:
                break
            m_x = mario.get_xy()[0]
            if m_x >= 150:
                break
        timetaken = time.time() - starttime
        os.system('fuser -k -TERM christmas_theme.ogg')

        os.system('tput reset')
        if mario.LIVES < 0:
            print("You are dead")
            os.system("espeak 'You are dead'")
            os.system("espeak 'You are dead'")
            os.system("espeak  'Suck it looser'")
        else:
            print("You finished this level")
            os.system("espeak Congratulations")

        print("Time Taken", int(timetaken), "Secconds")
        print("Score: ", mario.score - int(timetaken))
main()
