'''

contains all the symbols, constatnts
directions, etc

'''


# character representation of objects
WALL = "|"
MARIO = "M"
EMPTY = " "
ENEMY = "E"
FLOOR = "-"
BOX = "X"
COIN = "O"
CLOUD = "@"
END = 'F'

TYPES = {

    EMPTY: "Unassigned",
    WALL: "Wall",
    FLOOR: "Floor",
    BOX: "Box",
    MARIO: "Mario",
    ENEMY: "Enemy",
    COIN: "Coin",
    CLOUD: "Cloud",
    END: "End"
}

LIVES = [3, 10]


def getcc(char_ch):
    '''returns colors of corresponding characters'''
    try:
        color = "\033[44m"
        if char_ch == EMPTY:
            # light blue
            color = "\033[34;44m"
        elif char_ch == WALL:
            # yellow
            color = "\033[33;43m"
        elif char_ch == MARIO:
            # red
            color = "\033[31;41m"
        elif char_ch == FLOOR:
            # green
            color = "\033[32;42m"
        elif char_ch in (ENEMY, '[', ']'):
            # purple
            color = "\033[35;45m"
        elif char_ch == BOX:
            # bold white
            color = "\033[1;37;47m"
        elif char_ch == CLOUD:
            # bold and white
            color = "\033[1;37m"
        elif char_ch == COIN:
            # bold and yellow
            color = "\033[1;33m"
        elif char_ch == END:
            color = "\033[40;30m"
        return color + char_ch + "\033[30;47m"
    except KeyError:
        return char_ch
