'''The Mario Class

The Mario Class has all the variables and functionality of Mario
including the generation, movement and shooting.
This inherits Enemy and Shoot. '''
import numpy as np
import config

from objects import Object

class Person(Object):

    ''' MultiLevel Inheritance between Objects->Person->Mario/Enemy'''

    def __repr__(self):
        return "<Person : %s | (%d, %d)>" % (self.get_type(), self._x, self._y)

class Mario(Person):
    '''Class for Mario
    movement and behaviour are defined here'''

    def __init__(self, x, y, LIVES=config.LIVES[0]):
        super(Mario, self).__init__(x, y, config.MARIO)
        temp_skel = np.matrix([[config.EMPTY, config.MARIO, config.MARIO, config.EMPTY],
                               ['|', config.MARIO, config.MARIO, '|'],
                               ['|', config.MARIO, config.MARIO, '|'],
                               [config.EMPTY, '|', '|', config.EMPTY]])
        self.structure[:, :] = temp_skel
        self.LIVES = LIVES
        self.score = 0
        self.jump = 0
        del temp_skel

class Enemy(Person):
    '''Class for Enemies
    Methods for movement and behaviour'''
    def __init__(self, x, y, x_2):
        super(Enemy, self).__init__(x, y, config.ENEMY)
        temp_skel = np.matrix([['[', self._ch, self._ch, ']'],
                               [config.EMPTY, config.ENEMY, config.ENEMY, config.EMPTY],
                               [config.EMPTY, config.ENEMY, config.ENEMY, config.EMPTY],
                               [config.EMPTY, ']', '[', config.EMPTY]])
        self.x_1 = x
        self.x_2 = x_2
        self.dir = 'r'
        self.boss = False
        self.structure[:, :] = temp_skel
        del temp_skel

class Boss(Enemy):
    '''Class for Enemy Boss'''
    def __init__(self, x, y, x_2):
        super(Boss, self).__init__(x, y, config.ENEMY)
        temp_skel = np.matrix([['[', self._ch, self._ch, ']'],
                               [config.WALL, config.ENEMY, config.ENEMY, config.WALL],
                               [config.WALL, config.ENEMY, config.ENEMY, config.WALL],
                               [config.EMPTY, ']', '[', config.EMPTY]])
        self.x_1 = x
        self.x_2 = x_2
        self.dir = 'r'
        self.boss = True
        self.LIVES = 3
        self.structure[:, :] = temp_skel
        del temp_skel
